<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul',45);
            $tabe->string('isi',255);
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diperbarui');

            // foreign jawaban_Id_int
            $table->unsignedBigInteger('jawaban_tepat_id');
            $table->foreign('jawaban_tepat_id')->references('id')->on('jawabans');

            // foreign profiles_Id_Int
            $table->unsignedBigInteger('profile_id');
            $table->foreign('profile_id')->references('id')->on('profiles');
            



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaans');

        $table->foreign('profile_id')->references('id')->on('profiles');
        $table->unsignedBigInteger('profile_id');
        $table->foreign('jawaban_tepat_id')->references('id')->on('jawabans');  
        $table->unsignedBigInteger('jawaban_tepat_id');

            $table->date('tanggal_diperbarui');
            $table->date('tanggal_dibuat');
            $table->bigIncrements('id');
            $table->string('judul',45);

    }
}
